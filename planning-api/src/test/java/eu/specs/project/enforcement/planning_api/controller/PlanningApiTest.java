package eu.specs.project.enforcement.planning_api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.project.enforcement.planning_api.TestParent;
import eu.specs.project.enforcement.planning_api.mock.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:planning-api-context-test.xml")
public class PlanningApiTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(PlanningApiTest.class);

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
        this.objectMapper = new ObjectMapper();
    }

    @Test
    public void testSupplyChains() throws Exception {

        new ServiceManagerMock().init();
        new SlaManagerMock().init();
        new ImplementationMock().init();
        new CtpMock().init();

        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                this.getClass().getResourceAsStream("/SLATemplate-WebPool-SVA.xml"));
        String slaId = agreementOffer.getName();

        // input data for creating supply chain activity
        SupplyChainActivity scaInput = objectMapper.readValue(
                this.getClass().getResourceAsStream("/sca-input-data.json"), SupplyChainActivity.class);

        MvcResult result = mockMvc.perform(post("/sc-activities")
                .content(objectMapper.writeValueAsString(scaInput)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        SupplyChainActivity sca = objectMapper.readValue(
                result.getResponse().getContentAsString(), SupplyChainActivity.class);
        assertNotNull(sca.getId());
        assertEquals(sca.getState(), SupplyChainActivity.Status.COMPLETED);
        assertEquals(sca.getSlaId(), slaId);

        mockMvc.perform(get("/sc-activities/{0}", sca.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(sca.getId())));

        mockMvc.perform(get("/sc-activities/{0}/state", sca.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.state", is(SupplyChainActivity.Status.COMPLETED.name())));

        result = mockMvc.perform(get("/sc-activities/{0}/supply-chains", sca.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        List<SupplyChain> supplyChains = objectMapper.readValue(
                result.getResponse().getContentAsString(), new TypeReference<List<SupplyChain>>() {
                });
        assertEquals(supplyChains.size(), 4);
        SupplyChain supplyChain1 = supplyChains.get(0);
        assertEquals(supplyChain1.getSlaId(), slaId);
        assertEquals(supplyChain1.getScaId(), sca.getId());

        // get supply chain no. 1
        mockMvc.perform(get("/supply-chains/{0}", supplyChain1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(supplyChain1.getId())));

        // delete supply chain no. 1
        mockMvc.perform(delete("/supply-chains/{0}", supplyChain1.getId()))
                .andExpect(status().isNoContent());

        // check if supply chain was really deleted
        mockMvc.perform(get("/supply-chains/{0}", supplyChain1.getId()))
                .andExpect(status().isNotFound());

        // delete supply chain activity
        mockMvc.perform(delete("/sc-activities/{0}", sca.getId()))
                .andExpect(status().isNoContent());

        // check if supply chain activity exists
        mockMvc.perform(get("/sc-activities/{0}", sca.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testPlanningActivity() throws Exception {

        new ServiceManagerMock().init();
        new ImplementationMock().init();
        new SlaManagerMock().init();
        new MonipoliMock().init();
        new CtpMock().init();

        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                this.getClass().getResourceAsStream("/SLATemplate-WebPool-SVA.xml"));
        String slaId = agreementOffer.getName();

        // input data for creating supply chain activity
        SupplyChainActivity scaInput = objectMapper.readValue(
                this.getClass().getResourceAsStream("/sca-input-data.json"), SupplyChainActivity.class);

        MvcResult result = mockMvc.perform(post("/sc-activities")
                .content(objectMapper.writeValueAsString(scaInput)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        SupplyChainActivity sca = objectMapper.readValue(
                result.getResponse().getContentAsString(), SupplyChainActivity.class);
        assertNotNull(sca.getId());
        assertEquals(sca.getState(), SupplyChainActivity.Status.COMPLETED);
        assertEquals(sca.getSlaId(), slaId);

        mockMvc.perform(get("/sc-activities"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("sc-activity")))
                .andExpect(jsonPath("$.total", is(1)));

        mockMvc.perform(get("/sc-activities/{0}", sca.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(sca.getId())));

        mockMvc.perform(get("/sc-activities/{0}/state", sca.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.state", is(SupplyChainActivity.Status.COMPLETED.name())));

        result = mockMvc.perform(get("/sc-activities/{0}/supply-chains", sca.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        List<SupplyChain> supplyChains = objectMapper.readValue(
                result.getResponse().getContentAsString(), new TypeReference<List<SupplyChain>>() {
                });
        assertEquals(supplyChains.size(), 4);
        SupplyChain supplyChain1 = supplyChains.get(0);
        assertEquals(supplyChain1.getSlaId(), slaId);
        assertEquals(supplyChain1.getScaId(), sca.getId());

        // get all supply chains
        result = mockMvc.perform(get("/supply-chains"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("supply-chain")))
                .andExpect(jsonPath("$.total", is(4)))
                .andReturn();

        ResourceCollection resourceCollection = objectMapper.readValue(
                result.getResponse().getContentAsString(), ResourceCollection.class);

        // only one supply chain is selected, others should be removed
        for (int i = 1; i < resourceCollection.getTotal(); i++) {
            String supplyChainId = resourceCollection.getItemList().get(i).getId();
            mockMvc.perform(delete("/supply-chains/" + supplyChainId))
                    .andExpect(status().isNoContent());
        }

        // only one supply chain should remain
        mockMvc.perform(get("/supply-chains"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("supply-chain")))
                .andExpect(jsonPath("$.total", is(1)));

        // implement SLA - create planning activity
        Map<String, String> data = new HashMap<>();
        data.put("sla_id", slaId);
        result = mockMvc.perform(post("/plan-activities")
                .content(objectMapper.writeValueAsString(data)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        PlanningActivity planningActivity = objectMapper.readValue(
                result.getResponse().getContentAsString(), PlanningActivity.class);
        assertEquals(planningActivity.getSlaId(), slaId);

        // wait until the planning activity finishes
        Date startTime = new Date();
        do {
            logger.trace("Waiting for the planning activity to finish...");
            Thread.sleep(1000);
            result = mockMvc.perform(get("/plan-activities/{0}/status", planningActivity.getId()))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andReturn();
            Map<String, String> statusResp = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<Map<String, String>>() {
                    });
            PlanningActivity.Status status = PlanningActivity.Status.valueOf(statusResp.get("status"));

            if (status == PlanningActivity.Status.ACTIVE) {
                break;
            } else if (status == PlanningActivity.Status.ERROR) {
                fail("Planning activity failed.");
            }
            if (new Date().getTime() - startTime.getTime() > 15000) {
                fail("Timeout waiting for the planning activity to finish.");
            }
        } while (true);

        // get planning activity
        result = mockMvc.perform(get("/plan-activities/{0}", planningActivity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        planningActivity = objectMapper.readValue(result.getResponse().getContentAsString(), PlanningActivity.class);
        assertEquals(planningActivity.getState(), PlanningActivity.Status.ACTIVE);

        // get active plan id
        mockMvc.perform(get("/plan-activities/{0}/active", planningActivity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN))
                .andExpect(content().string(planningActivity.getActivePlanId()));

        // get all planning activities using filter
        mockMvc.perform(get("/plan-activities?slaId={0}&state={1}&offset=0&limit=10",
                slaId, PlanningActivity.Status.ACTIVE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.resource", is("plan-activity")))
                .andExpect(jsonPath("$.total", is(1)));

        // delete planning activity
        mockMvc.perform(delete("/plan-activities/{0}", planningActivity.getId()))
                .andExpect(status().isNoContent());

        // get deleted planning activity
        mockMvc.perform(get("/plan-activities/{0}", planningActivity.getId()))
                .andExpect(status().isNotFound());
    }
}