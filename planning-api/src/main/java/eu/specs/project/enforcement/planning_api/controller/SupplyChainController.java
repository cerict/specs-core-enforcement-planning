package eu.specs.project.enforcement.planning_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.enforcement.planning.core.service.SupplyChainService;
import eu.specs.project.enforcement.planning_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(value = "/supply-chains")
public class SupplyChainController {

    @Autowired
    private SupplyChainService supplyChainService;

    @RequestMapping(value = "/{scId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public SupplyChain getSupplyChain(@PathVariable("scId") String scId) {
        SupplyChain supplyChain = supplyChainService.getSupplyChain(scId);
        if (supplyChain == null) {
            throw new ResourceNotFoundException("The supply chain with given ID cannot be found.");
        } else {
            return supplyChain;
        }
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getCollection(
            @RequestParam(value = "slaId", required = false) String slaId,
            HttpServletRequest request) {
        List<SupplyChain> supplyChains = supplyChainService.findBySlaId(slaId);
        ResourceCollection collection = new ResourceCollection();
        collection.setResource("supply-chain");
        collection.setTotal(supplyChains.size());
        collection.setMembers(supplyChains.size());

        for (SupplyChain supplyChain : supplyChains) {
            String uri = request.getRequestURL() + "/" + supplyChain.getId();
            collection.addItem(new ResourceCollection.Item(supplyChain.getId(), uri));
        }
        return collection;
    }

    @RequestMapping(value = "/{scId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteSupplyChain(@PathVariable("scId") String scId) {
        SupplyChain supplyChain = supplyChainService.getSupplyChain(scId);
        if (supplyChain == null) {
            throw new ResourceNotFoundException(String.format("The supply chain %s cannot be found.", scId));
        } else {
            supplyChainService.delete(supplyChain);
        }
    }
}
