package eu.specs.project.enforcement.planning_api;

import java.util.ArrayList;
import java.util.List;

public class ItemCollection {
    private List<Item> items;

    public ItemCollection() {
        items = new ArrayList<>();
    }

    public void addItem(String id, String href) {
        items.add(new Item(id, href));
    }

    public List<Item> getItems() {
        return items;
    }

    public static class Item {
        private String id;
        private String href;

        public Item(String id, String href) {
            this.id = id;
            this.href = href;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }
}
