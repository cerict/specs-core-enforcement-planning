package eu.specs.project.enforcement.planning_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.Reconfiguration;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.service.ReconfigService;
import eu.specs.project.enforcement.planning_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "/reconfigs")
public class ReconfigController {
    private static final String RECONFIG_PATH = "/reconfigs/{id}";

    @Autowired
    private ReconfigService reconfigService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public Reconfiguration reconfigure(@RequestBody Reconfiguration reconfig) throws PlanningException {
        return reconfigService.reconfigure(reconfig);
    }

    @RequestMapping(value = "/{reconfigId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Reconfiguration getReconfig(@PathVariable("reconfigId") String reconfigId) {
        Reconfiguration reconfig = reconfigService.findById(reconfigId);
        if (reconfig == null) {
            throw new ResourceNotFoundException(String.format(
                    "The reconfiguration with ID %s cannot be found.", reconfigId));
        } else {
            return reconfig;
        }
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAll(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        ReconfigService.ReconfigsFilter filter = new ReconfigService.ReconfigsFilter();
        filter.setSlaId(slaId);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<Reconfiguration> reconfigs = reconfigService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (Reconfiguration reconfig : reconfigs) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(RECONFIG_PATH)
                    .buildAndExpand(reconfig.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(reconfig.getId(), uri.toString()));
        }
        collection.setTotal(reconfigs.size());
        collection.setMembers(reconfigs.size());
        return collection;
    }
}
