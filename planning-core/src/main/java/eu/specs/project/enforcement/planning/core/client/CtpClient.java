package eu.specs.project.enforcement.planning.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

@Component
public class CtpClient {
    private static String CTP_PATH = "/sla-creation/{id}";
    private static final Logger logger = LogManager.getLogger(CtpClient.class);

    private WebTarget ctpTarget;

    @Autowired
    public CtpClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        ctpTarget = client.target(appConfig.getCtpAddress());
    }

    public void notifySlaCreation(String slaId) throws PlanningException {
        logger.debug("Notifying CTP about the SLA {}...", slaId);

        try {
            Response response = ctpTarget
                    .path(CTP_PATH)
                    .resolveTemplate("id", slaId)
                    .request()
                    .put(Entity.text(""));

            if (response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL) {
                logger.debug("CTP has been notified successfully.");
            } else {
                logger.error("Failed to notify CTP. Invalid response code: {}", response.getStatus());
                throw new PlanningException("Invalid response code: " + response.getStatus());
            }

        } catch (Exception e) {
            throw new PlanningException(String.format("Failed to notify CTP about the SLA %s creation: %s",
                    slaId, e.getMessage()), e);
        }
    }
}
