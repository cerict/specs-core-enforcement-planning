package eu.specs.project.enforcement.planning.core.service;

import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.enforcement.planning.core.client.ImplementationClient;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.processor.PlanningActivityProcessor;
import eu.specs.project.enforcement.planning.core.repository.PlanningActivityRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class PlanningActivityService {
    private static final Logger logger = LogManager.getLogger(PlanningActivityService.class);
    public static final int EXECUTOR_NUMBER_OF_THREADS = 3;
    private ExecutorService executorService;

    @Autowired
    private PlanningActivityRepository paRepository;

    @Autowired
    private PlanningActivityProcessor paProcessor;

    @Autowired
    private ImplementationClient implClient;

    public PlanningActivityService() {
        executorService = Executors.newFixedThreadPool(EXECUTOR_NUMBER_OF_THREADS);
    }

    public PlanningActivity createPlanningActivity(SupplyChain supplyChain) throws PlanningException {
        logger.debug("Creating planning activity for the SLA {}...", supplyChain.getSlaId());
        final PlanningActivity planningActivity = new PlanningActivity();
        planningActivity.setId(UUID.randomUUID().toString());
        planningActivity.setCreationTime(new Date());
        planningActivity.setState(PlanningActivity.Status.CREATED);
        planningActivity.setSlaId(supplyChain.getSlaId());
        planningActivity.setSupplyChainId(supplyChain.getId());
        planningActivity.setSupplyChain(supplyChain);
        paRepository.save(planningActivity);

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                paProcessor.processPlanningActivity(planningActivity);
            }
        });

        logger.debug("Planning activity {} has been created successfully.", planningActivity.getId());
        return planningActivity;
    }

    public PlanningActivity getPlanningActivity(String paId) {
        return paRepository.findById(paId);
    }

    public List<PlanningActivity> find(PlanningActivityFilter filter) {
        return paRepository.find(filter);
    }

    public PlanningActivity findBySlaId(String slaId) throws PlanningException {
        PlanningActivityFilter filter = new PlanningActivityFilter();
        filter.setSlaId(slaId);
        List<PlanningActivity> planningActivities = paRepository.find(filter);
        if (planningActivities.isEmpty()) {
            return null;
        } else if (planningActivities.size() == 1) {
            return getPlanningActivity(planningActivities.get(0).getId());
        } else {
            throw new PlanningException(String.format(
                    "Multiple planning activities found for the SLA %s.", slaId));
        }
    }

    public void deletePlanningActivity(PlanningActivity planningActivity) throws PlanningException {
        logger.debug("Deleting planning activity {}...", planningActivity.getId());
        if (planningActivity.getImplActivityId() != null) {
            implClient.deleteImplActivity(planningActivity.getImplActivityId());
        }
        paRepository.delete(planningActivity);
        logger.debug("Planning activity has been deleted successfully.");
    }

    public static class PlanningActivityFilter {
        private String slaId;
        private PlanningActivity.Status state;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public PlanningActivity.Status getState() {
            return state;
        }

        public void setState(PlanningActivity.Status state) {
            this.state = state;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}