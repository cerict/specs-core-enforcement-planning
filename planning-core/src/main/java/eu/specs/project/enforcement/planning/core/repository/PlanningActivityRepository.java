package eu.specs.project.enforcement.planning.core.repository;

import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.project.enforcement.planning.core.service.PlanningActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PlanningActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public PlanningActivity findById(String paId) {
        return mongoTemplate.findById(paId, PlanningActivity.class);
    }

    public List<PlanningActivity> find(PlanningActivityService.PlanningActivityFilter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getState() != null) {
            query.addCriteria(Criteria.where("state").is(filter.getState().name()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, PlanningActivity.class);
    }

    public void save(PlanningActivity planningActivity) {
        mongoTemplate.save(planningActivity);
    }

    public void delete(PlanningActivity planningActivity) {
        mongoTemplate.remove(planningActivity);
    }
}
