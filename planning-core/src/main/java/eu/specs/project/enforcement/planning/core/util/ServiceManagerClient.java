package eu.specs.project.enforcement.planning.core.util;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@Component
public class ServiceManagerClient {
    private static String SEC_MECHANISM_PATH = "/security-mechanisms/{id}";
    private static final Logger logger = LogManager.getLogger(ServiceManagerClient.class);

    private WebTarget serviceManagerTarget;

    @Autowired
    public ServiceManagerClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        serviceManagerTarget = client.target(appConfig.getServiceManagerApiAddress());
    }

    public SecurityMechanism retrieveSecurityMechanism(String mechanismId) throws PlanningException {
        try {
            logger.debug("Retrieving security mechanism {}...", mechanismId);
            SecurityMechanism mechanism = serviceManagerTarget
                    .path(SEC_MECHANISM_PATH)
                    .resolveTemplate("id", mechanismId)
                    .request(MediaType.APPLICATION_JSON)
                    .get(SecurityMechanism.class);

            logger.debug("Mechanism has been retrieved successfully.");
            return mechanism;
        } catch (Exception e) {
            throw new PlanningException(String.format("Failed to obtain security mechanism %s: %s",
                    mechanismId, e.getMessage()), e);
        }
    }
}
