package eu.specs.project.enforcement.planning.core.repository;

import eu.specs.datamodel.enforcement.SupplyChainActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SCActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public SupplyChainActivity findById(String scaId) {
        return mongoTemplate.findById(scaId, SupplyChainActivity.class);
    }

    public List<SupplyChainActivity> getAll() {
        return mongoTemplate.findAll(SupplyChainActivity.class);
    }

    public void save(SupplyChainActivity sca) {
        mongoTemplate.save(sca);
    }

    public void delete(SupplyChainActivity sca) {
        mongoTemplate.remove(sca);
    }
}
