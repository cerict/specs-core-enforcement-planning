package eu.specs.project.enforcement.planning.core.processor;

import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Solver {
    private static final Logger logger = LogManager.getLogger(Solver.class);

    private static String WEBPOOL_CAPABILITY = "WEBPOOL";
    private static String SVA_CAPABILITY = "SVA";
    private static String DBB_CAPABILITY = "DBB";
    private static String E2EE_CAPABILITY = "E2EE";
    private static String OSSEC_CAPABILITY = "OSSEC";
    private static String AAA_CAPABILITY = "AAA";
    private static String TLS_CAPABILITY = "TLS";

    private static String METRIC_M1 = "level_of_redundancy_m1";
    private static String METRIC_M2 = "level_of_diversity_m2";
    private static String METRIC_M13 = "basic_scan_frequency_m13";
    private static String METRIC_M22 = "extended_scan_frequency_m22";
    private static String METRIC_M23 = "up_report_frequency_m23";
    private static String METRIC_M24 = "pen_testing_activated_m24";

    private Map<String, SLOType> metricSloMapping;
    private Map<String, CapabilityType> capabilities;

    public Solver(Map<String, SLOType> metricSloMapping, Map<String, CapabilityType> capabilities) {

        this.metricSloMapping = metricSloMapping;
        this.capabilities = capabilities;
    }

    public void allocateResources(SupplyChain supplyChain) throws PlanningException {
        logger.debug("Calculating allocations for supply chain {}...", supplyChain.getId());
        logger.debug("Capabilities: {}", capabilities.keySet());

        // set allocations
        int vmNum = 0;
        List<SupplyChain.Allocation> allocations = new ArrayList<>();

        if (capabilities.containsKey(WEBPOOL_CAPABILITY)) {

            Integer m1 = null;
            Integer m2 = null;

            if (metricSloMapping.containsKey(METRIC_M1)) {
                m1 = Integer.parseInt((String) metricSloMapping.get(METRIC_M1).getSLOexpression().getOneOpExpression().getOperand());
            } else {
                throw new PlanningException(String.format("Metric '%s' is not defined in the SLA.", METRIC_M1));
            }

            if (metricSloMapping.containsKey(METRIC_M2)) {
                m2 = Integer.parseInt((String) metricSloMapping.get(METRIC_M2).getSLOexpression().getOneOpExpression().getOperand());
            } else {
                m2 = 1;
            }

            if(capabilities.containsKey(TLS_CAPABILITY)){
                allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("wp_haproxy","tls_prober","tls_terminator",
                		"tls_terminator_configurator","tls_terminator_controller")));
            }
            else{
                allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("wp_haproxy")));
            }
            

            if (m2 == 1) {
                for (int i = 0; i < m1; i++) {
                    allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("wp_apache")));
                }
            } else if (m2 == 2) {
                for (int i = 0; i < m1; i++) {
                    if (i % 2 == 0) {
                        allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("wp_apache")));
                    } else {
                        allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("wp_nginx")));
                    }
                }
            } else {
                throw new PlanningException("Invalid value of the metric level_of_diversity_m2: " + m2);
            }
        }

        if (capabilities.containsKey(DBB_CAPABILITY)) {
            allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("DBB_main_db")));
            allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("DBB_backup_db")));
            allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("DBB_main_server")));
            allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("DBB_backup_server")));
            allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("DBB_auditor", "DBB_monitoring_adapter")));
        }

        if (capabilities.containsKey(E2EE_CAPABILITY) && !capabilities.containsKey(DBB_CAPABILITY)) {
            throw new PlanningException("E2EE capability requires the DBB capability.");
        }

        if (capabilities.containsKey(SVA_CAPABILITY)) {

            // find place for sva_dashboard
            SupplyChain.Allocation svaDashboardAlloc = null;
            for (SupplyChain.Allocation allocation : allocations) {
                if (allocation.getComponentIds().contains("wp_haproxy")) {
                    svaDashboardAlloc = allocation;
                    break;
                }
            }

            if (svaDashboardAlloc == null) {
                svaDashboardAlloc = new SupplyChain.Allocation(0, vmNum++, new ArrayList<String>());
                allocations.add(svaDashboardAlloc);
            }

            svaDashboardAlloc.addComponentId("sva_dashboard");

            for (SupplyChain.Allocation allocation : allocations) {
                if (allocation != svaDashboardAlloc) {
                    allocation.addComponentId("sva_enforcement");
                    allocation.addComponentId("sva_monitoring");
                }
            }

            if (metricSloMapping.containsKey(METRIC_M13) ||
                    metricSloMapping.containsKey(METRIC_M22) ||
                    metricSloMapping.containsKey(METRIC_M23) ||
                    metricSloMapping.containsKey(METRIC_M24)) {

                for (SupplyChain.Allocation allocation : allocations) {
                    if (allocation != svaDashboardAlloc) {
                        allocation.addComponentId("sva_openscap");
                    }
                }
            }
        }
        
        if(capabilities.containsKey(AAA_CAPABILITY)){
        	allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("aaa")));
        }
        

        if (capabilities.containsKey(OSSEC_CAPABILITY)) {
            for (SupplyChain.Allocation allocation : allocations) {
                allocation.addComponentId("ossec_agent");
            }

            allocations.add(new SupplyChain.Allocation(0, vmNum++, Arrays.asList("ossec_server")));
        }

        supplyChain.setAllocations(allocations);
        supplyChain.setVmNumber(vmNum);
    }
}