package eu.specs.project.enforcement.planning.core.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${sla-manager-api.address}")
    private String slaManagerApiAddress;

    @Value("${service-manager-api.address}")
    private String serviceManagerApiAddress;

    @Value("${implementation-api.address}")
    private String implementationApiAddress;

    @Value("${monipoli.address}")
    private String monipoliAddress;

    @Value("${implementation-activity.timeout}")
    private int implActivityTimeout;

    @Value("${ctp-api.address}")
    private String ctpAddress;

    public String getSlaManagerApiAddress() {
        return slaManagerApiAddress;
    }

    public String getServiceManagerApiAddress() {
        return serviceManagerApiAddress;
    }

    public String getImplementationApiAddress() {
        return implementationApiAddress;
    }

    public String getMonipoliAddress() {
        return monipoliAddress;
    }

    public int getImplActivityTimeout() {
        return implActivityTimeout;
    }

    public String getCtpAddress() {
        return ctpAddress;
    }
}
