package eu.specs.project.enforcement.planning.core.exceptions;

public class PlanningException extends Exception {

    public PlanningException() {
        super();
    }

    public PlanningException(String message) {
        super(message);
    }

    public PlanningException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
