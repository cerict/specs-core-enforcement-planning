package eu.specs.project.enforcement.planning.core.processor;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.common.Annotation;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.project.enforcement.planning.core.client.SlaManagerClient;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.repository.SCActivityRepository;
import eu.specs.project.enforcement.planning.core.repository.SupplyChainRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class SCActivityProcessor {
    private static final Logger logger = LogManager.getLogger(SCActivityProcessor.class);

    @Autowired
    private SCActivityRepository scaRepository;

    @Autowired
    private SupplyChainRepository scRepository;

    @Autowired
    private SlaManagerClient slaManagerClient;

    public void buildSupplyChains(SupplyChainActivity sca) {
        logger.debug("buildSupplyChains(scaId={}) started...", sca.getId());
        sca.setState(SupplyChainActivity.Status.RUNNING);
        scaRepository.save(sca);

        try {
            buildSupplyChainsImpl(sca);

            sca.setState(SupplyChainActivity.Status.COMPLETED);
            scaRepository.save(sca);
            logger.debug("buildSupplyChains() finished successfully.");
        } catch (Exception e) {
            logger.error("Failed to build supply chains: " + e.getMessage(), e);
            sca.setState(SupplyChainActivity.Status.ERROR);
            sca.addAnnotation(new Annotation("error", e.toString(), new Date()));
            scaRepository.save(sca);
        }
    }

    private void buildSupplyChainsImpl(SupplyChainActivity sca) throws PlanningException {
        String slaId = sca.getSlaId();
        // check if supply chain set already exists for the same SLA (in case buildSupplyChains request has been repeated for the same SLA)
        List<SupplyChain> oldSupplyChains = scRepository.findBySlaId(slaId);
        if (!oldSupplyChains.isEmpty()) {
            // delete old supply chains
            logger.debug("Removing obsolete supply chains: {} found.", oldSupplyChains.size());
            for (SupplyChain supplyChain : oldSupplyChains) {
                try {
                    scRepository.delete(supplyChain);
                } catch (Exception e) {
                    throw new PlanningException(String.format("Failed to delete obsolete supply chain %s: %s",
                            supplyChain.getId(), e.getMessage()), e);
                }
                logger.debug("Obsolete supply chain {} corresponding to the same SLA {} has been deleted.",
                        supplyChain.getId(), slaId);
            }
        }

        AgreementOffer sla = slaManagerClient.retrieveSla(slaId);

        ServiceDescriptionTerm serviceDescriptionTerm = null;
        Map<String, ServiceProperties> servicePropertiesTerms = new HashMap<>();
        Map<String, GuaranteeTerm> guaranteeTerms = new HashMap<>();

        for (Term term : sla.getTerms().getAll().getAll()) {
            if (term instanceof ServiceDescriptionTerm) {
                serviceDescriptionTerm = (ServiceDescriptionTerm) term;
            } else if (term instanceof ServiceProperties) {
                ServiceProperties serviceProperties = (ServiceProperties) term;
                servicePropertiesTerms.put(serviceProperties.getName(), serviceProperties);
            } else if (term instanceof GuaranteeTerm) {
                GuaranteeTerm guaranteeTerm = (GuaranteeTerm) term;
                guaranteeTerms.put(guaranteeTerm.getName(), guaranteeTerm);
            }
        }

        // extract capabilities
        logger.debug("Extracting capabilities from the SLA template...");
        Map<String, CapabilityType> capabilities = new HashMap<>();
        for (CapabilityType capability : serviceDescriptionTerm.getServiceDescription().getCapabilities().getCapability()) {
            capabilities.put(capability.getId().toUpperCase(), capability);
            logger.debug("Capability: {}", capability.getId());
        }

        // cloud resources
        ServiceDescriptionType.ServiceResources serviceResources =
                serviceDescriptionTerm.getServiceDescription().getServiceResources().get(0);

        List<SupplyChain.CloudResource> cloudResourceList = new ArrayList<>();
        for (ServiceDescriptionType.ServiceResources.ResourcesProvider resourcesProvider : serviceResources.getResourcesProvider()) {

            for (ServiceDescriptionType.ServiceResources.ResourcesProvider.VM vm : resourcesProvider.getVM()) {
                SupplyChain.CloudResource cloudResource = new SupplyChain.CloudResource();
                cloudResource.setProviderId(resourcesProvider.getId());
                cloudResource.setZoneId(resourcesProvider.getZone());
                cloudResource.setAppliance(vm.getAppliance());
                cloudResource.setVmType(vm.getHardware());
                cloudResourceList.add(cloudResource);
            }
        }

        // variable->metric mapping
        Map<String, String> variableMetricMapping = new HashMap<>();
        Pattern capabilityPattern = Pattern.compile("//specs:capability\\[@id='([^']+)'\\]");
        for (String servicePropName : servicePropertiesTerms.keySet()) {
            Matcher m = capabilityPattern.matcher(servicePropName);
            if (m.find()) {
                ServiceProperties serviceProp = servicePropertiesTerms.get(servicePropName);
                for (Variable variable : serviceProp.getVariableSet().getVariables()) {
                    variableMetricMapping.put(variable.getName(), variable.getMetric());
                }
            } else {
                throw new PlanningException("Unexpected SLA template format: " + servicePropName);
            }
        }

        // metric->SLO mapping
        Map<String, SLOType> metricSloMapping = new HashMap<>();
        for (String guaranteeTermName : guaranteeTerms.keySet()) {
            Matcher m = capabilityPattern.matcher(guaranteeTermName);
            if (m.find()) {
                GuaranteeTerm guaranteeTerm = guaranteeTerms.get(guaranteeTermName);
                for (SLOType slo : guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList().getSLO()) {
                    if (variableMetricMapping.containsKey(slo.getMetricREF())) {
                        String metric = variableMetricMapping.get(slo.getMetricREF());
                        metricSloMapping.put(metric, slo);
                    }
                }
            } else {
                throw new PlanningException("Unexpected SLA template format: " + guaranteeTermName);
            }
        }

        Solver solver = new Solver(metricSloMapping, capabilities);

        for (SupplyChain.CloudResource cloudResource : cloudResourceList) {
            SupplyChain supplyChain = new SupplyChain();
            supplyChain.setId(UUID.randomUUID().toString());
            supplyChain.setSlaId(sca.getSlaId());
            supplyChain.setCreationTime(new Date());
            supplyChain.setScaId(sca.getId());
            supplyChain.setCloudResource(cloudResource);

            // set security mechanisms
            for (String mechanismId : sca.getSecurityMechanisms()) {
                supplyChain.addSecurityMechanisms(mechanismId);
            }

            solver.allocateResources(supplyChain);

            scRepository.save(supplyChain);
            logger.debug("Supply chain {} has been created successfully.", supplyChain.getId());
        }
    }
}

