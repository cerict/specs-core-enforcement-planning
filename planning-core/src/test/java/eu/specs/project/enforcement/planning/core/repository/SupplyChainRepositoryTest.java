package eu.specs.project.enforcement.planning.core.repository;

import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.enforcement.planning.core.TestParent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:planning-core-context-test.xml")
public class SupplyChainRepositoryTest extends TestParent {

    @Autowired
    private SupplyChainRepository scRepository;

    @Test
    public void test() throws Exception {
        SupplyChain supplyChain = new SupplyChain();
        supplyChain.setId("supply_chain_id");
        supplyChain.setSlaId("sla_id");
        supplyChain.setScaId("sca_id");

        scRepository.save(supplyChain);
        SupplyChain supplyChain1 = scRepository.find(supplyChain.getId());
        assertEquals(supplyChain1.getId(), supplyChain.getId());

        List<SupplyChain> supplyChains = scRepository.findBySlaId(supplyChain.getSlaId());
        assertEquals(supplyChains.get(0).getId(), supplyChain.getId());

        supplyChains = scRepository.findByScaId(supplyChain.getScaId());
        assertEquals(supplyChains.get(0).getId(), supplyChain.getId());

        scRepository.delete(supplyChain);
        assertNull(scRepository.find(supplyChain.getId()));
    }
}