package eu.specs.project.enforcement.planning.core.mock;

import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class CtpMock {

    public void init() throws IOException {

        stubFor(put(urlPathMatching("/adaptor/sla-creation/[\\w-]+"))
                .willReturn(aResponse()
                        .withStatus(200)));
    }
}
